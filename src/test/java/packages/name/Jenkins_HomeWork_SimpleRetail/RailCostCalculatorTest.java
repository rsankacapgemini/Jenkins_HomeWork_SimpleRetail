package packages.name.Jenkins_HomeWork_SimpleRetail;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

// Writing a parameterized test for Air Cost Calculator
@RunWith(Parameterized.class)
public class RailCostCalculatorTest {
	private double expectedValue;
	private Item item;
	

	public RailCostCalculatorTest(double expectedValue, Item item) {
		super();
		this.expectedValue = expectedValue;
		this.item = item;
	}

	RailCostCalculator railCostCalculator;
	@Before
	public void setup() {
		railCostCalculator = new RailCostCalculator();
	}
	@Test
	 public void testCalculateShippingCost() {
		assertEquals(expectedValue, railCostCalculator.calculateShippingCost(item),0.001);
	}
	@Parameters
	public static Collection<Object[]> testData() {
		Object[][] data = new Object[][] { {10, new Item(312321101516L, "Hot Tub", 9899.99, 793.41, "RAIL")},
			{5,(new Item(322322202488L, "HeavyMac Laptop", 4555.79, 4.08, "RAIL"))}};
			return Arrays.asList(data);
	}
}
