package packages.name.Jenkins_HomeWork_SimpleRetail;

import java.util.ArrayList;
import java.util.Map;

public interface CostCaluculator {
	 double calculateShippingCost(Item item);
}
