package packages.name.Jenkins_HomeWork_SimpleRetail;

public class Item {
	private long upc;
	private String description;
	private double price;
	private double weight;
	private String shippingMethod;
	public Item(long upc, String description, double price, double weight, String shippingMethod) {
		super();
		this.upc = upc;
		this.description = description;
		this.price = price;
		this.weight = weight;
		this.shippingMethod = shippingMethod;
	}
	public long getUpc() {
		return upc;
	}
	public void setUpc(long upc) {
		this.upc = upc;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public String getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	@Override
	public String toString() {
		return "Item [upc=" + upc + ", description=" + description + ", price=" + price + ", weight=" + weight
				+ ", shippingMethod=" + shippingMethod + "]";
	}
	

}
