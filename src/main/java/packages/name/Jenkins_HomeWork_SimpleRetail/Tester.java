package packages.name.Jenkins_HomeWork_SimpleRetail;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.xml.crypto.Data;


public class Tester {
   
	public static void main(String[] args) {		
		ArrayList<Item> itemsList = new ArrayList<Item>();
		itemsList.add(new Item(567321101987L, "CD � Pink Floyd, Dark Side Of The Moon", 19.99, 0.58, "AIR"));
		itemsList.add(new Item(567321101986L, "CD � Beatles, Abbey Road", 17.99, 0.61, "GROUND"));
		itemsList.add(new Item(567321101985L, "CD � Queen, A Night at the Opera", 20.49, 0.55, "AIR"));
		itemsList.add(new Item(567321101984L, "CD � Beatles, Abbey Road", 23.88, 0.50, "GROUND"));
		itemsList.add(new Item(467321101899L, "iPhone - Waterproof Case", 9.75, 0.73, "AIR"));
		itemsList.add(new Item(477321101878L, "iPhone -  Headphones", 17.25, 3.21, "GROUND"));
		itemsList.add(new Item(312321101516L, "Hot Tub", 9899.99, 793.41, "RAIL"));
		itemsList.add(new Item(322322202488L, "HeavyMac Laptop", 4555.79, 4.08, "RAIL"));


		Tester tester = new Tester();
		tester.processShipping(itemsList);

	}

	private void processShipping(ArrayList<Item> itemsList) {
		// TODO Auto-generated method stub
		HashMap<Long, Double> priceMap = new HashMap<Long, Double>();
		Context context = null;

		for (Item item : itemsList) {

			if (item.getShippingMethod().equalsIgnoreCase("AIR")) {
				context = new Context(new AirCostCalculator());

			} else if(item.getShippingMethod().equalsIgnoreCase("RAIL")) {
				context = new Context(new RailCostCalculator());
			} else {
				context = new Context(new GroundCostCalculator());
			}
			priceMap.put(item.getUpc(), context.calculateShippingCost(item));

		}

		printInvoice(itemsList, priceMap);
	}

	private void printInvoice(ArrayList<Item> itemsList, HashMap<Long, Double> priceMap) {
		// TODO Auto-generated method stub
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println("*****SHIPMENT REPORT*****                   " + dateFormat.format(date));
		System.out.println("------------------------------------------------------------------");
		System.out.format("%10s%40s%10s%10s%20s%20s", "UPC", "Description", "Price", "Weight", "Ship Method",
				"Shipping Cost");
		Collections.sort(itemsList, new Comparator<Item>() {

			public int compare(Item o1, Item o2) {
				// TODO Auto-generated method stub
				if (o1.getUpc() > o2.getUpc())
					return 1;
				return -1;
			}

		});
		for (Item items : itemsList) {
			System.out.println();
			System.out.format("%10s%40s%10s%10s%20s%20s", items.getUpc(), items.getDescription(), items.getPrice(),
					items.getWeight(), items.getShippingMethod(), priceMap.get(items.getUpc()));

		}
		
		double totalCost = 0;
		for (Double cost : priceMap.values()) {
			totalCost += cost;
		}
		System.out.println();
		System.out.println("Total Shipping Cost:        " + Math.floor(totalCost * 100) / 100);

	}

}
